#ifndef __RELATION_STATS_DECL_HH__
#define __RELATION_STATS_DECL_HH__

#include "relation-decl.hh"

namespace CuddAbstraction {

/**
 * @brief Class to compute relation statistics.
 */
class RelationStats {
private:
  /// The relation to take statistics from
  const Relation& rel_;

private:
  /// Returns the underlying BDD of the relation representation
  BDD asBdd(void) const;

public:
  RelationStats(void) = delete;
  /// Constructor from relation @a r
  RelationStats(const Relation& r) : rel_(r) {}
  /// Copy constructor
  RelationStats(const RelationStats& st) : rel_(st.rel_) {}
  /**
   * @brief Computes the number of nodes used to represent the relation
   */
  int nodes(void) const;
  /**
   * @brief Computes the density of the relation representation
   *
   * The density is the ratio between the number of tuples in the relation with
   * respect to the number of BDD nodes used to represent them.
   */
  double density(void) const;

  /// Computes the cardinality of the relation
  double cardinality(void) const;
  /**
   * @brief  Computes the number of nodes shared by the relations in @a rels
   *
   * @warning Assumes all the relations are in the same manager
   */
  static int sharingSize(const SpaceManager& home,
                         const std::vector<Relation>& rels);
};

std::ostream& operator<<(std::ostream& os, const RelationStats& st);
}
#endif