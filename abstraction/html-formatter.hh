#ifndef __RELATION_HTML_FORMATTER_HH_
#define __RELATION_HTML_FORMATTER_HH_

#include "html-formatter-decl.hh"

namespace CuddAbstraction {
///////////////////
// HtmlFormatter //
///////////////////

inline HtmlFormatter::HtmlFormatter(const Relation& r, std::ostream& os,
                                    DomainGroup* dg, const std::string& bgcolor,
                                    bool standalone)
    : Formatter(r, os, dg)
    , standalone_(standalone)
    , bgcolor_(bgcolor) {}

inline void HtmlFormatter::standalone(bool st) { standalone_ = st; }

inline void HtmlFormatter::bgcolor(const std::string& color) {
  bgcolor_ = color;
}

inline Formatter::Decoder HtmlFormatter::getDecoder(void) const {
  return[&](const NumericTuple & et)->std::vector<std::string> {
    std::vector<std::string> decoded(rel_.schema().arity());
  decoded[0] = "<tr bgcolor=\"";
  decoded[0] += bgcolor_;
  decoded[0] += "\">";
  for (size_t i = 0; i < rel_.schema().arity(); i++) {
    decoded[i] += "<td>";
    if (!dg_)
      decoded[i] += std::to_string(et[i]);
    else
      decoded[i] +=
          dg_->getValueEncodedBy(et[i], rel_.schema().asColumnDomains()[i]);
    decoded[i] += "</td>";
  }
  decoded[rel_.arity() - 1] += "</tr>";
  return decoded;
};
}

inline void HtmlFormatter::printSchema(void) const {
  os_ << "<tr>";
  for (const Attribute& att : rel_.schema().asColumnDomains())
    os_ << "<th>" << att.name() << "</th>";
  os_ << "</tr>";
}

inline void HtmlFormatter::printTuples(const SpaceManager& home) const {
  Formatter::decodeAndPrintTuples(home, getDecoder(), false);
}

inline void HtmlFormatter::print(const SpaceManager& home) const {
  if (standalone_)
    os_ << "<html><body>";
  os_ << "<table border=\" 1 \" cellpadding=\" 4 \">";
  printSchema();
  printTuples(home);
  os_ << "</table>";
  if (standalone_)
    os_ << "</body></html>";
}

inline void printHtml(const SpaceManager& home, const Relation& rel,
                      std::ostream& os, DomainGroup* dg) {
  HtmlFormatter f(rel, os, dg);
  f.print(home);
}
}

#endif