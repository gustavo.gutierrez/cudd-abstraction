#include <ctime>
#include "logging.hh"

#ifdef LOGGING_ENABLED

// Initialize the logging library
_INITIALIZE_EASYLOGGINGPP

////////////////////////////////
// Static data initialization //
////////////////////////////////
unsigned int OperationLogger::nextOpId_ = 0;
std::mutex OperationLogger::mutex_;

unsigned int OperationLogger::getIdForOperation(void) {
  std::lock_guard<std::mutex> guard(mutex_);
  unsigned int id = nextOpId_;
  nextOpId_++;
  return id;
}

void initLogging(const std::string& conf) {
  if (el::base::utils::File::pathExists(conf.c_str(), true)) {
    el::Configurations config(conf);
    el::Loggers::reconfigureAllLoggers(config);
  } else {
    el::Configurations config;
    config.parseFromText("*GLOBAL:\n ENABLED=false\n TO_STANDARD_OUTPUT=false");
    el::Loggers::reconfigureAllLoggers(config);
    std::cerr << "Logging engine failed to initialize from file. Disabled.\n";
  }
}

// el::base::type::StoragePointer sharedLoggingRepository() {
//   return el::Helpers::storage();
// }

#endif
