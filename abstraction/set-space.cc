#include "set-space-decl.hh"
#include "set-space-basic.hh"
#include "format.h"

#include <cstdlib>
#include <cassert>
#include <iostream>

#include "min-nonzero.h"
#include "agg-to-relation.h"

namespace CuddAbstraction {

inline void checkReturnValue(const DdNode* result) {
  assert(result != 0 && "CUDD error returned");
}

void SpaceManager::printLevels(BDD cube) const {
  while (!cube.IsOne()) {
    unsigned int level = var2level(cube);
    fmt::print("{} ", level);
    cube = high(cube);
  }
  fmt::print("\n");
}

ADD SpaceManager::findMinNonzero(ADD f) const {
  DdManager* mgr = f.manager();
  DdNode* result;
  result = minNonzero(mgr, f.getNode());
  checkReturnValue(result);
  return ADD(*this, result);
}

BDD SpaceManager::augmentedWithDiscriminants(
    ADD a, BDD cube, const BDDVarDomain& indices) const {
  DdManager* mgr = a.manager();
  DdNode* result =
      convert(mgr, a.getNode(), cube.getNode(), indices.data(), indices.size());
  return BDD(*this, result);
}
}
