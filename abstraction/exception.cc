#include "exception-decl.hh"

namespace CuddAbstraction {
Exception::Exception(const char* location, const char* information) throw() {
  int j = 0;
  while ((*location != 0) && (j < li_max))
    li[j++] = *(location++);
  if (j < li_max)
    li[j++] = ':';
  if (j < li_max)
    li[j++] = ' ';
  while ((*information != 0) && (j < li_max))
    li[j++] = *(information++);
  li[j] = 0;
}
const char* Exception::what(void) const throw() { return &li[0]; }
}