#ifndef __GROUND_HH__
#define __GROUND_HH__

#include "ground-decl.hh"
#include <cassert>
#include <vector>

namespace CuddAbstraction {

namespace internal {
// The implementation in this namespace are based on the boolean vector
// implementation provided by the Buddy BDD library. More precisely in the
// files bvec.h and bvec.c.
//
// The changes that I made are only to use the C++ api and present a more
// readable code. All the credit on the algorithms goes to Jorn Lind-Nielsen.
// A copy of buddy can be downloaded from http://http://buddy.sourceforge.net

/// Type definition for a vector of BDD's
typedef std::vector<BDD> BVec;

inline BVec buildVector(const SpaceManager& home, size_t size, bool val) {
  if (val)
    return BVec(size, home.bddOne());
  return BVec(size, home.bddZero());
}

inline BVec domainToBDDVector(const SpaceManager& home, const Attribute& d) {
  assert(d.reprSize() > 0);
  BVec v(d.reprSize(), home.bddZero());
  auto& indices = d.getIndices();

  for (size_t i = 0; i < d.reprSize(); i++) {
    v[i] = home.bddVar(indices.at(i));
  }

  return v;
}

/**
 * @brief Returns a vector of BDD variables encoding the constant @a c. The
 *  variables used for the representation are the ones of domain @a d.
 *
 * The value is represented in such a way that the first element of the vector
 *  is the LSB and the last one is the MSB.
 */
inline BVec vecConstant(const SpaceManager& home, size_t size,
                        const IntegerDatum& c) {
  assert(c.numBits() <= size && "Not enough space to represent the constant");
  auto v = buildVector(home, size, false);

  for (unsigned int i = 0; i < c.numBits(); i++) {
    if (c.getBit(i))
      v[i] = home.bddOne();
    else
      v[i] = home.bddZero();
  }
  return v;
}

inline BVec vecAdd(const SpaceManager& home, const BVec& left,
                   const BVec& right) {
  assert(left.size() == right.size());

  if (left.size() == 0)
    return buildVector(home, 0, false);

  BDD carry = home.bddZero();
  auto res = buildVector(home, left.size(), false);

  for (size_t i = 0; i < left.size(); i++) {
    res[i] = left[i] ^ right[i] ^ carry;
    carry = (left[i] & right[i]) | (carry & (left[i] | right[i]));
  }
  return res;
}

inline BDD vecLessThanOrEqual(const SpaceManager& home, const BVec& left,
                              const BVec& right) {
  assert(left.size() == right.size());

  if (left.size() == 0)
    return home.bddZero();

  BDD p = home.bddOne();

  for (size_t i = 0; i < left.size(); i++) {
    p = (!left[i] & right[i]) | ((!(left[i] ^ right[i])) & p);
  }

  return p;
}

inline BDD vecLessThan(const SpaceManager& home, const BVec& left,
                       const BVec& right) {
  assert(left.size() == right.size());

  if (left.size() == 0)
    return home.bddZero();

  BDD p = home.bddZero();

  for (size_t i = 0; i < left.size(); i++) {
    p = (!left[i] & right[i]) | ((!(left[i] ^ right[i])) & p);
  }

  return p;
}

inline BDD vecEqual(const SpaceManager& home, const BVec& left,
                    const BVec& right) {
  assert(left.size() == right.size());

  if (left.size() == 0)
    return home.bddZero();

  BDD p = home.bddOne();

  for (size_t i = 0; i < left.size(); i++) {
    p = !(left[i] ^ right[i]) & p;
  }

  return p;
}

inline bool sameSize(const Attribute& d0, const Attribute& d1) {
  return d0.reprSize() == d1.reprSize();
}

inline bool sameSize(const Attribute& d0, const Attribute& d1,
                     const Attribute& d2) {
  return (d0.reprSize() == d1.reprSize()) && (d1.reprSize() == d2.reprSize());
}
}

inline Relation bitwisePlus(const SpaceManager& home, const Schema& d) {
  if (d.arity() != 3)
    throw ExpectingTernaryRelationDomain("bitwisePlus");

  auto dom = d.asColumnDomains();
  const Attribute& left = dom.at(0), right = dom.at(1), result = dom.at(2);

  if (!internal::sameSize(left, right, result))
    throw InvalidRepresentationSize("bitwisePlus");

  auto vLeft = internal::domainToBDDVector(home, left);
  auto vRight = internal::domainToBDDVector(home, right);
  auto vResult = internal::domainToBDDVector(home, result);

  auto tmp = internal::vecAdd(home, vLeft, vRight);

  BDD r = home.bddOne();
  for (size_t i = 0; i < left.reprSize(); i++)
    r &= !(vResult[i] ^ tmp[i]);

  return Relation(r, d);
}

inline Relation bitwisePlus(const SpaceManager& home, const Attribute& left,
                            const IntegerDatum& c, const Attribute& result) {

  if (!internal::sameSize(left, result))
    throw InvalidRepresentationSize("bitwisePlus");

  if (c.numBits() > left.reprSize())
    throw InsufficientDomainSize("bitwisePlus");

  auto vLeft = internal::domainToBDDVector(home, left);
  auto vConst = internal::vecConstant(home, left.reprSize(), c);
  auto vResult = internal::domainToBDDVector(home, result);

  auto tmp = internal::vecAdd(home, vLeft, vConst);

  BDD r = home.bddOne();
  for (size_t i = 0; i < left.reprSize(); i++)
    r &= !(vResult[i] ^ tmp[i]);

  Schema d(home, {left, result});
  return Relation(r, d);
}

inline Relation arithPlus(const SpaceManager& home, const Schema& d) {
  if (d.arity() != 3)
    throw ExpectingTernaryRelationDomain("arithPlus");

  // Compute the bitwise plus relation
  Relation groundPlus = bitwisePlus(home, d);

  auto dom = d.asColumnDomains();
  assert(dom.size() == 3);
  const Attribute& left = dom.at(0), right = dom.at(1), result = dom.at(2);

  if (!internal::sameSize(left, right, result))
    throw InvalidRepresentationSize("arithPlus");

  // In order to detect overflow on the addition we use the fact that in the
  // resulting relation result must be greater or equal than the other two
  // columns. Hence we generate the corresponding greater-or-equal ground
  // relations and intersect with them.
  //
  // TODO: This is probably not the most efficient way so a proper
  // implementation
  // that builds the right BDD from scratch will perform better.
  Schema d0(home, {result, left});
  Relation resultGreaterOrEqualThanLeft = greaterOrEqual(home, d0);

  Schema d1(home, {result, right});
  Relation resultGreaterOrEqualThanRight = greaterOrEqual(home, d1);

  BDD r = groundPlus.data_ & resultGreaterOrEqualThanLeft.data_ &
          resultGreaterOrEqualThanRight.data_;
  return Relation(r, d);
}

inline Relation arithPlus(const SpaceManager& home, const Attribute& left,
                          const IntegerDatum& c, const Attribute& result) {

  if (!internal::sameSize(left, result))
    throw InvalidRepresentationSize("arithPlus");

  if (c.numBits() > left.reprSize())
    throw InsufficientDomainSize("arithPlus");

  // Compute the bitwise plus relation
  Relation groundPlus = bitwisePlus(home, left, c, result);

  // In order to detect overflow on the addition we use the fact that in the
  // resulting relation result must be greater or equal than the other two
  // columns. Hence we generate the corresponding greater-or-equal ground
  // relations and intersect with them.
  //
  // TODO: This is probably not the most efficient way so a proper
  // implementation
  // that builds the right bdd from scratch will perform better.
  Schema d0(home, {result, left});
  Relation resultGreaterOrEqualThanLeft = greaterOrEqual(home, d0);

  BDD r = groundPlus.data_ & resultGreaterOrEqualThanLeft.data_;
  return Relation(r, groundPlus.schema());
}

inline Relation lessOrEqual(const SpaceManager& home, const Schema& d) {
  if (d.arity() != 2)
    throw ExpectingBinaryRelationDomain("lessOrEqual");

  auto dom = d.asColumnDomains();
  const Attribute& left = dom.at(0), right = dom.at(1);

  if (!internal::sameSize(left, right))
    throw InvalidRepresentationSize("lessOrEqual");

  auto vLeft = internal::domainToBDDVector(home, left);
  auto vRight = internal::domainToBDDVector(home, right);

  BDD r = internal::vecLessThanOrEqual(home, vLeft, vRight);
  return Relation(r, d);
}

inline Relation lessOrEqual(const SpaceManager& home, const Attribute& dom,
                            const IntegerDatum& c) {

  if (c.numBits() > dom.reprSize())
    throw InsufficientDomainSize("lessOrEqual");

  auto vDom = internal::domainToBDDVector(home, dom);
  auto vConst = internal::vecConstant(home, dom.reprSize(), c);

  BDD r = internal::vecLessThanOrEqual(home, vDom, vConst);
  Schema d(home, {dom});
  return Relation(r, d);
}

inline Relation lessThan(const SpaceManager& home, const Schema& d) {
  if (d.arity() != 2)
    throw ExpectingBinaryRelationDomain("lessThan");

  auto dom = d.asColumnDomains();
  const Attribute& left = dom.at(0), right = dom.at(1);

  if (!internal::sameSize(left, right))
    throw InvalidRepresentationSize("lessThan");

  auto vLeft = internal::domainToBDDVector(home, left);
  auto vRight = internal::domainToBDDVector(home, right);

  BDD r = internal::vecLessThan(home, vLeft, vRight);
  return Relation(r, d);
}

inline Relation lessThan(const SpaceManager& home, const Attribute& dom,
                         const IntegerDatum& c) {

  if (c.numBits() > dom.reprSize())
    throw InsufficientDomainSize("lessThan");

  auto vDom = internal::domainToBDDVector(home, dom);
  auto vConst = internal::vecConstant(home, dom.reprSize(), c);

  BDD r = internal::vecLessThan(home, vDom, vConst);
  Schema d(home, {dom});
  return Relation(r, d);
}

inline Relation greaterOrEqual(const SpaceManager& home, const Schema& d) {
  if (d.arity() != 2)
    throw ExpectingBinaryRelationDomain("greaterOrEqual");
  return lessThan(home, d).complement();
}

inline Relation greaterOrEqual(const SpaceManager& home, const Attribute& dom,
                               const IntegerDatum& c) {

  if (c.numBits() > dom.reprSize())
    throw InsufficientDomainSize("greaterOrEqual");
  return lessThan(home, dom, c).complement();
}

inline Relation greaterThan(const SpaceManager& home, const Schema& d) {
  if (d.arity() != 2)
    throw ExpectingBinaryRelationDomain("greaterThan");
  return lessOrEqual(home, d).complement();
}

inline Relation greaterThan(const SpaceManager& home, const Attribute& dom,
                            const IntegerDatum& c) {
  if (c.numBits() > dom.reprSize())
    throw InsufficientDomainSize("greaterThan");
  return lessOrEqual(home, dom, c).complement();
}

inline Relation equal(const SpaceManager& home, const Schema& d) {
  if (d.arity() != 2)
    throw ExpectingBinaryRelationDomain("equal");

  auto dom = d.asColumnDomains();
  const Attribute& left = dom.at(0), right = dom.at(1);

  if (!internal::sameSize(left, right))
    throw InvalidRepresentationSize("equal");

  auto vLeft = internal::domainToBDDVector(home, left);
  auto vRight = internal::domainToBDDVector(home, right);

  BDD r = internal::vecEqual(home, vLeft, vRight);

  return Relation(r, d);
}

inline Relation different(const SpaceManager& home, const Schema& d) {
  if (d.arity() != 2)
    throw ExpectingBinaryRelationDomain("equal");
  return equal(home, d).complement();
}
}
#endif
