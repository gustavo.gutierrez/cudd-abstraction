#include <iostream>
#include <gtest/gtest.h>

#include "bdd-abstraction.hh"

using namespace std;
using namespace CuddAbstraction;

TEST(AggRelvalue, construction) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(2), "A");
    Attribute b(home, makeDomain(2), "B");

    Schema ab(home, {a, b});
    AggRelation f(home, ab);

    f.add(home, 3, 1, 1);
    f.add(home, 2, 1, 0);
    f.add(home, 1, 0, 0);

    // f.toDot(home, "add.dot", "A");
  }
}

TEST(AggRelvalue, aggregatedAsAttribute) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(2), "A");
    Attribute b(home, makeDomain(4), "B");

    Schema ab(home, {a, b});
    AggRelation f(home, ab);

    f.add(home, 2, 1, 1);
    f.add(home, 2, 1, 0);
    f.add(home, 2, 0, 2);
    f.add(home, 2, 0, 0);

    Attribute c(home, makeDomain(4), "C");
    Relation r = f.aggregatedAsAttribute(home, c);

    // r.toDot(home,"aggres.dot","agg");
    print(home, r, std::cout);
    f.toDot(home, "add.dot", "A");
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(AggRelvalue, constructionDomainGroup) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(2), "A");
    Attribute b(home, makeDomain(2), "B");
    DomainGroup dg;
    dg << a << b;

    Schema ab(home, {a, b});
    AggRelation f(home, ab);

    insert(home, dg, f, 3, "foo", "bar");
    insert(home, dg, f, 2, "foo", "zoe");
    insert(home, dg, f, 1, "zoe", "zoe");

    Attribute c(home, makeDomain(4), "C");
    Relation r = f.aggregatedAsAttribute(home, c);

    EXPECT_EQ(r.cardinality(), 3);

    print(home, r, std::cout, &dg);
  }
}

TEST(AggRelvalue, grouping) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(2), "A");
    Attribute b(home, makeDomain(2), "B");
    Attribute c(home, makeDomain(2), "C");
    Attribute d(home, makeDomain(2), "D");

    Schema abcd(home, {a, b, c, d});
    Relation r(home, abcd);

    r.add(home, 1, 0, 1, 1);
    r.add(home, 1, 1, 1, 1);
    r.add(home, 0, 0, 1, 1);
    r.add(home, 1, 0, 1, 0);
    r.add(home, 1, 1, 1, 0);
    r.add(home, 0, 0, 0, 0);

    AggRelation g = AggRelation::groupBy(home, r, {c, d});
    // g.toDot(home, "add.dot", "G");
  }
}

TEST(AggRelvalue, minNonezero) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(2), "A");
    Attribute b(home, makeDomain(2), "B");
    Attribute c(home, makeDomain(4), "C");

    Schema abc(home, {a, b, c});
    Relation f(home, abc);
    f.add(home, {0, 0, 0});
    f.add(home, {0, 0, 1});

    f.add(home, {0, 1, 1});
    f.add(home, {0, 1, 2});
    f.add(home, {0, 1, 0});

    {
      AggRelation g = AggRelation::groupBy(home, f, {a, b});
      EXPECT_EQ(g.minNonzero(home), 2);
    }

    f.add(home, {1, 1, 1});
    {
      AggRelation g = AggRelation::groupBy(home, f, {a, b});
      EXPECT_EQ(g.minNonzero(home), 1);
    }
  }
}

TEST(AggRelvalue, grouping2) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(4), "A");
    Attribute c1(home, makeDomain(4), "B");
    Attribute c2(home, makeDomain(4), "C");

    Schema c0c1c2(home, {c0, c1, c2});
    Relation f(home, c0c1c2);
    f.add(home, {0, 0, 0});
    f.add(home, {0, 0, 1});
    f.add(home, {3, 0, 1});
    f.add(home, {3, 1, 1});
    f.add(home, {3, 1, 2});
    f.add(home, {3, 1, 0});
    f.add(home, {3, 2, 0});
    f.add(home, {3, 2, 1});

    // print(home, f, std::cout);
    AggRelation g = AggRelation::groupBy(home, f, {c0, c1});

    EXPECT_EQ(g.max(home), 3);
    EXPECT_EQ(g.min(home), 0);

    // There is only one tuple with g 1: <3,0,1>
    Relation sub1 = g.filterInterval(home, 1, 1);
    EXPECT_EQ(sub1.joinWith(home, f).cardinality(), 1);

    // There are three tuples with rank 3
    Relation sub3 = g.filterInterval(home, 3, 3);
    EXPECT_EQ(sub3.joinWith(home, f).cardinality(), 3);

    // There are seven tuples with rankings from 2 to 3
    Relation sub23 = g.filterInterval(home, 2, 3);
    EXPECT_EQ(sub23.joinWith(home, f).cardinality(), 7);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(AggRelvalue, aggJoin) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(8), "A");
    Attribute b(home, makeDomain(8), "B");

    Schema sch(home, {a, b});
    Relation f(home, sch);

    f.add(home, {0, 0});
    f.add(home, {3, 0});
    f.add(home, {3, 1});
    f.add(home, {2, 5});

    // print(home, f, std::cout);

    Schema schAgg(home, {b});
    AggRelation g(home, schAgg);

    g.add(home, 10, {0});
    g.add(home, 7, {1});

    EXPECT_EQ(g.cardinality(), 2);

    AggRelation join = g.joinWith(home, f);

    EXPECT_EQ(join.cardinality(),3);
    EXPECT_EQ(join.minNonzero(home),7);
    EXPECT_EQ(join.max(home),10);

    // Attribute agg(home, makeDomain(16), "Agg");
    // Relation r = join.aggregatedAsAttribute(home, agg);
    // print(home, r, std::cout);
  }
  EXPECT_EQ(0, home.zeroReferences());
}
