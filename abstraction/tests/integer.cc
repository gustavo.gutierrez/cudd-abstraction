#include <gtest/gtest.h>
#include "bdd-abstraction.hh"

// using namespace std;
using namespace CuddAbstraction;

TEST(ArbitraryInteger, creation) {
  IntegerDatum x(4);
  EXPECT_EQ(x.numBits(), 3u);

  IntegerDatum y(16);
  EXPECT_EQ(y.numBits(), 5u);
}

TEST(ArbitraryInteger, bitOrdering) {
  IntegerDatum y(8);
  EXPECT_EQ(y.numBits(), 4u);
  for (size_t i = 0; i < y.numBits(); i++)
    std::cout << "Bit[" << i << "]: " << y.getBit(i) << std::endl;
}
