#ifndef __RELATION_FORMATTER_DECL_HH__
#define __RELATION_FORMATTER_DECL_HH__

#include "relation-decl.hh"
#include "string-handler-decl.hh"

namespace CuddAbstraction {

/**
 * @brief Relation formatter to carry out the printing of relations
 */
class Formatter {
protected:
  /// Relation to output
  const Relation rel_;
  /// Stream to output
  std::ostream& os_;
  /// domain group with the encodings
  DomainGroup* dg_;

  /// Maximum width
  int maxWidth_;
  /// Type definition for NumericTuple decoder
  typedef std::function<std::vector<std::string>(const NumericTuple& t)>
  Decoder;

private:
  /// Columns widths
  std::vector<int> widths_;

public:
  /**
   * @brief Constructs a formatter for relation @a r
   *
   * If a domain group @a dg is supplied, it is used to print the encodings
   * instead of the values stored in the relation.
   */
  Formatter(const Relation& r, std::ostream& os, DomainGroup* dg = nullptr,
            int maxWidth = 40);

private:
  /**
   * @brief Computes the column widths required fro printing the relation
   */
  void widthsForPrinting(void);
  /**
   * @brief Returns the length of the complete table including the separators
   */
  int totalLength(void) const;
  /**
   * @brief Dummy tuple decoder that transforms integers into their string
   *     representation
   */
  static std::vector<std::string> identityDecode(const NumericTuple& t);
  /**
   * @brief Returns a decoder function based on the domain group @a dg_
   */
  Decoder getDecoder(void) const;

protected:
  /**
   * @brief Prints a textual representation of the tuples of the relation to @a
   *     os
   *
   * - @a decode is a function that is applied to every element in a tuple of
   *   the relation @a rel.
   */
  void decodeAndPrintTuples(const SpaceManager& home, Decoder dec,
                            bool newLine = true) const;

public:
  /**
   * @brief Prints a textual representation of the schema of the relation to @a
   *     os
   */
  void printSchema(void) const;
  /// Outputs a line of characters @a c with the width of the relation
  void printSeparator(char c) const;
  /**
   * @brief Prints the tuples in the relation to @a os
   */
  void printTuples(const SpaceManager& home) const;
  /**
   * @brief Outputs a representation of the relation to @a os
   */
  virtual void print(const SpaceManager& home) const;
};

/**
 * @brief Outputs the relation @a r to output stream @a os
 */
void print(const SpaceManager& home, const Relation& rel, std::ostream& os,
           DomainGroup* dg = nullptr, int maxWidth = 40);
}

#endif