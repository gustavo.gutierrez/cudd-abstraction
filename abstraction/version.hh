#ifndef __CUDD_ABSTRACTION_VERSION_HH__
#define __CUDD_ABSTRACTION_VERSION_HH__

namespace CuddAbstraction {
/**
 * @brief Returns the git hash of the current HEAD
 */
const char* getVersionHash(void);

/**
 * @brief Returns the build date of the library
 */
const char* buildDate(void);
}
#endif