#ifndef __AGG_TO_RELATION_H_
#define __AGG_TO_RELATION_H_

#include <stdio.h>
#include <cudd.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Converts the ADD representation of an aggregated relation into a
 *     relation.
 *
 * Given the ADD @a f that represents an aggregated relation, this function
 * creates a relation by encoding the discriminants in the ADD using the boolean
 * variables in @a vars.
 *
 * - @a vars contains the indexes of the variables that will be used to encode
 *   each discriminant and has @a size elements. The i-th bit of the number is
 *   encoded using the i-th variable of @a vars.
 *
 * @warn It is assumed that all the discriminants can be encoded in the provided
 *     variables and no check about that is performed.
 */
DdNode* convert(DdManager* mgr, DdNode* f, DdNode* cube, const int* vars,
                int size);

#ifdef __cplusplus
}
#endif

#endif
