#include "set-space-decl.hh"
#include "set-space-basic.hh"
#include <cudd.h>
#include <cuddInt.h>
#include <cuddObj.hh>
#include <cstdlib>
/**
 * @brief This file contains the different implementations of unique
 *     abstraction of relations represented as BDDs.
 *
 * The implementations available are:
 *
 * 1- A direct implementation in C that uses existential abstraction.
 * Definition is provided in unique.c
 *
 * 2- A MTBDD implementation.
 *
 */

#define UNIQUE_ABSTRACT_C_IMP 1

//////////////////////
// C implementation //
//////////////////////
extern "C" {
/**
 * @brief Default implementation of the unique abstraction.
 */
DdNode* uniqueAbstract(DdManager* mgr, DdNode* f, DdNode* cube);
/**
 * @brief Unique abstraction implementation.
 */
DdNode* uniqueAbstractFast(DdManager* mgr, DdNode* f, DdNode* cube);
}

//////////////////
// Using MTBDDs //
//////////////////

namespace CuddAbstraction {
/**
 * @bried Unique abstraction using MTBDDs.
 *
 * Abstracts @a cube uniquely from @a f.
 */
BDD uniqueAbstractADD(BDD f, BDD cube) {
  // Convert f into an ADD
  ADD g = f.Add();
  // Convert cube into an ADD cube (Not sure if this is actually required)
  ADD c = cube.Add();
  // compute the abstraction of c using arithmetic addition
  ADD abstracted = g.ExistAbstract(c);
  // Select a BDD with 1 as discriminant
  return abstracted.BddInterval(1, 1);
}
}

///////////////////////////////////////
// Unique abstraction implementation //
///////////////////////////////////////
namespace CuddAbstraction {
inline DdManager* checkSameManager(BDD f, BDD g) {
  DdManager* fMgr = f.manager();
  DdManager* gMgr = g.manager();

  if (fMgr != gMgr)
    std::abort();
  return fMgr;
}

//////////////////
// SpaceManager //
//////////////////
BDD SpaceManager::uniqueAbstract(BDD f, BDD cube) const {
  uniqueAbstractCalls_++;
#ifdef UNIQUE_ABSTRACT_C_IMP
  DdManager* mgr = checkSameManager(f, cube);
  DdNode* result;
  result = ::uniqueAbstract(mgr, f.getNode(), cube.getNode());
  checkReturnValue(result);
  return BDD(*this, result);
#else
  return uniqueAbstractADD(f, cube);
#endif
}

BDD SpaceManager::uniqueAbstractFast(BDD f, BDD cube) const {
  uniqueAbstractCalls_++;
  fastUniqueAbstractCalls_++;

  DdManager* mgr = checkSameManager(f, cube);
  DdNode* result;
  result = ::uniqueAbstractFast(mgr, f.getNode(), cube.getNode());
  checkReturnValue(result);
  return BDD(*this, result);
}

bool SpaceManager::canUseUniqueAbstractFast(BDD notAbstracted,
                                            BDD toAbstract) const {
  // assumes a positive cube
  // assumes toAbstract is not a constant
  unsigned int toAbstractTop = var2level(toAbstract);
  while (!notAbstracted.IsOne()) {
    unsigned int currentLevel = var2level(notAbstracted);
    if (currentLevel > toAbstractTop)
      return false;
    notAbstracted = high(notAbstracted);
  }
  return true;
}

/////////////////////////////////////
// Statistics (unique abstraction) //
/////////////////////////////////////

int SpaceManager::uniqueAbstractCalls() const { return uniqueAbstractCalls_; }

int SpaceManager::fastUniqueAbstractCalls() const {
  return uniqueAbstractCalls_;
}

unsigned long SpaceManager::uniqueAbstractRecCalls() const {
  return getManager()->unique_abstraction_total_calls;
}

unsigned long SpaceManager::fatsUniqueAbstractRecCalls() const {
  return getManager()->unique_abstraction_optimized_calls;
}

unsigned long SpaceManager::existAbstractRecCalls() const {
  return getManager()->exist_abstract_total_calls;
}

unsigned long SpaceManager::uniqueExistAbstractRecCalls() const {
  return getManager()->exist_abstract_calls;
}

unsigned long SpaceManager::iteRecCalls() const {
  return getManager()->ite_calls;
}
}