#include "agg-to-relation.h"
#include <assert.h>
#include <cuddInt.h>
#include <util.h>

extern char* encodeInteger(int d);

DdNode* encodeDiscriminantCache(DdManager* mgr, DdNode* f, DdNode* g) {
  // Function that does nothing but allowing us to us the CUDD cache to store
  // already computed discriminants
  assert(0 == 1);
  return NULL;
}
/**
 * @brief Returns the BDD encoding of @a discriminant in the variables @a vars.
 */
DdNode* encodeDiscriminant(DdManager* mgr, DdNode* discriminant, DdNode* cube,
                           const int* vars, int size) {
  // Use a cache to prevent the encoding of the same terminal several times.
  // DdNode* d = Cudd_addConst(mgr, discriminant);
  // DdNode* result =
  //     cuddCacheLookup2(mgr, encodeDiscriminantCache, discriminant, cube);
  // if (result != NULL) {
  // printf("Cache hit!!!!\n");
  // return result;
  // }

  // Discriminant is guaranteed to be a constant
  char* encoding = encodeInteger(cuddV(discriminant));
  int len = strlen(encoding);

  DdNode* result = Cudd_ReadOne(mgr);
  Cudd_Ref(result);
  DdNode* var, *tmp;
  for (int i = 0; i < size; i++) {
    // printf("Encoding val %c\n", encoding[len - 1 - i]);
    var = Cudd_bddIthVar(mgr, vars[i]);
    if (encoding[len - 1 - i] == '0')
      var = Cudd_Not(var);

    tmp = Cudd_bddAnd(mgr, result, var);
    Cudd_Ref(tmp);
    Cudd_RecursiveDeref(mgr, result);
    result = tmp;
  }
  free(encoding);

  Cudd_Deref(result);
  // Insert result in the cache
  // cuddCacheInsert2(mgr, encodeDiscriminantCache, discriminant, cube, result);
  return result;
}
/*
  The implementation of the following function is an adaptation of
  Cudd_addMonadicApply present in the file cuddAddApply.c of the CUDD BDD
  library.
 */
DdNode* convertRecur(DdManager* dd, DdNode* f, DdNode* cube, const int* vars,
                     int size) {
  /**
   * @todo An optimization would be to store the BDD encoding of the
   *     discriminants in such a way that if the same discriminant is reached
   *     several times it is encoded only once.
   */
  DdNode* res, *ft, *fe, *T, *E;
  unsigned int index;

  /* Check terminal cases. */
  if (cuddIsConstant(f)) {
    if (f == DD_ZERO(dd))
      // Return the logical zero
      return Cudd_Not(Cudd_ReadOne(dd));
    else
      return encodeDiscriminant(dd, f, cube, vars, size);
  }

  /* Check cache. */
  res = cuddCacheLookup2(dd, encodeDiscriminantCache, f, cube);

  // res = cuddCacheLookup1(dd, op, f);
  if (res != NULL)
    return (res);

  /* Recursive step. */
  index = f->index;
  ft = cuddT(f);
  fe = cuddE(f);

  T = convertRecur(dd, ft, cube, vars, size);
  if (T == NULL)
    return (NULL);
  cuddRef(T);

  E = convertRecur(dd, fe, cube, vars, size);
  if (E == NULL) {
    Cudd_RecursiveDeref(dd, T);
    return (NULL);
  }
  cuddRef(E);

  if (Cudd_IsComplement(T)) {
    res = (T == E) ? Cudd_Not(T)
                   : cuddUniqueInter(dd, index, Cudd_Not(T), Cudd_Not(E));
    if (res == NULL) {
      Cudd_RecursiveDeref(dd, T);
      Cudd_RecursiveDeref(dd, E);
      return NULL;
    }
    res = Cudd_Not(res);
  } else {
    res = (T == E) ? T : cuddUniqueInter(dd, index, T, E);
    if (res == NULL) {
      Cudd_RecursiveDeref(dd, T);
      Cudd_RecursiveDeref(dd, E);
      return NULL;
    }
  }

  cuddDeref(T);
  cuddDeref(E);

  /* Store result. */
  cuddCacheInsert2(dd, encodeDiscriminantCache, f, cube, res);

  // cuddCacheInsert1(dd, op, f, res);

  return (res);
}

DdNode* convert(DdManager* mgr, DdNode* f, DdNode* cube, const int* vars,
                int size) {
  DdNode* res;
  do {
    mgr->reordered = 0;
    res = convertRecur(mgr, f, cube, vars, size);
  } while (mgr->reordered == 1);
  return res;
}
