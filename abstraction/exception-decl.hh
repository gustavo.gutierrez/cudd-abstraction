#ifndef __EXCEPTION_DECL_HH__
#define __EXCEPTION_DECL_HH__

#include <exception>
#include <utility>

namespace CuddAbstraction {
/**
 * \brief Class representing a exception in the abstraction layer.
 */
class Exception : public std::exception {
private:
  static const int li_max = 127;
  char li[li_max + 1];

public:
  // Prevent default construction
  Exception(void) = delete;
  /// Constructor from an error message \a m
  Exception(const char* location, const char* information) throw();
  /// Return information
  virtual const char* what(void) const throw();
};

/**
 * @brief Funtion to state run-time assertions.
 */
template <typename Exception, typename Condition, typename... Args>
inline void Assert(Condition condition, Args... args) {
  if (!condition)
    throw Exception(std::forward<Args>(args)...);
}

/**
 * @brief Exception thrown when an operation does somthing wrong and there is no
 * a more convenient exception.
 */
class GenericException : public Exception {
public:
  GenericException(void) = delete;
  GenericException(const char* location)
      : Exception(location, "Generic exception fix-me") {}
};

/**
 * @brief Exception thrown when an operation requires to attributes to have the
 *     same representation size.
 */
class InvalidRepresentationSize : public Exception {
public:
  InvalidRepresentationSize(void) = delete;
  InvalidRepresentationSize(const char* location)
      : Exception(location, "Attempt to operate attributes with different "
                            "representation sizes") {}
};

/**
 * @brief Exception thrown by operations that need to represent elements on a
 *     non big enough domain.
 */
class InsufficientDomainSize : public Exception {
public:
  InsufficientDomainSize(void) = delete;
  InsufficientDomainSize(const char* location)
      : Exception(location,
                  "Attempt to represent value on a not big enough domain") {}
};
}

#endif
