#ifndef __SET_SPACE_HH__
#define __SET_SPACE_HH__

#include "set-space-decl.hh"
#include "format.h"

#include <cassert>
#include <iostream>
#include <vector>

namespace CuddAbstraction {

inline size_t VarAllocator::linearStart() const {
  /**
   * The linear region starts after the interleaved one. As such, the index of
   * the first variable in this region is the one after the last index of the
   * interleaved region.
   *
   * The first index for the interleaved variable is 0.
   */
  return blockSize * numBlocks;
}

inline void VarAllocator::markAllocated(size_t index) {
  freeVars_.reset(index);
}

inline size_t VarAllocator::blockOf(size_t index) const {
  Assert<InvalidAllocation>(index < linearStart(), "VarAllocator::blockOf()");
  return index % blockSize;
}

inline size_t VarAllocator::nextAvailableInBlock(size_t block) const {
  Assert<InvalidAllocation>(block < numBlocks,
                            "VarAllocator::nextAvailableInBlock()");
  size_t start = block * blockSize;
  for (size_t i = start; i < (start + blockSize); i++) {
    if (freeVars_[i])
      return i;
  }
  Assert<InvalidAllocation>(false, "VarAllocator::nextAvailableInBlock()");
  return start + blockSize;
}

inline std::vector<int> VarAllocator::allocInterleaved(size_t length) {
  /**
   * The allocation of @a length variables will use one variable per block.
   *
   * @warn The following implementation assumes there is room in the first block
   *     and in the subsequent ones. That is not always true and hence the
   *     allocation here is sub-optimal.
   */
  Assert<InvalidAllocation>(length <= numBlocks,
                            "VarAllocator::allocInterleaved(length)");
  std::vector<int> vars(length, 0);
  for (size_t i = 0; i < length; i++) {
    size_t varIndex = nextAvailableInBlock(i);
    vars[i] = varIndex;
    markAllocated(varIndex);
  }
  return vars;
}

inline std::vector<int> VarAllocator::allocLinear(size_t length) {
  std::vector<int> vars(length, 0);
  size_t i = 0;
  for (size_t varIndex = linearStart();
       varIndex < freeVars_.size() && i < length; varIndex++) {
    if (freeVars_[varIndex]) {
      vars[i] = varIndex;
      markAllocated(varIndex);
      i++;
    }
  }
  Assert<FailedAllocation>(i == length, "VarAllocator::allocateLinear()");
  return vars;
}

inline std::vector<int> VarAllocator::alloc(size_t length, bool interleaved) {
  if (interleaved)
    return allocInterleaved(length);
  return allocLinear(length);
}

inline std::vector<int> VarAllocator::alloc(const std::vector<int>& vars) {
  LOG_OPERATION_NARGS("VAlloc::external");
  for (auto i : vars) {
    Assert<InvalidAllocation>(
        (i >= 0 && static_cast<size_t>(i) < freeVars_.size()),
        "VarAllocator::alloc(vector<int>)");
    if (!freeVars_[i]) {
      // The variable is already in use, silently ignore that fact.
      fmt::print(stderr, "Requested variable: {} is already in use.\n", i);
    } else {
      freeVars_.set(i);
    }
  }
  return vars;
}

inline void VarAllocator::printDebugInfo(void) const {
  auto interleavedSize = blockSize * numBlocks;
  size_t linearSize = ALLOCATOR_SIZE - interleavedSize;
  fmt::print("Initializing allocator.\n\tBlock size: "
             "{}\n\tNumber of blocks: {}\n\tBDD vars in "
             "interleaved region: {}\n\tBDD vars in linear region: {}\n",
             blockSize, numBlocks, interleavedSize, linearSize);
  fmt::print(
      "Total variables: {}\n\tI.R indices {}...{}\n\tL.R indices {}..{}\n",
      freeVars_.size(), 0, interleavedSize - 1, interleavedSize,
      freeVars_.size() - 1);
}
/*
 * Space manager
 */
inline bool SpaceManager::haveVarsInCommon(const BDD& r, const BDD& s) {
  LOG_OPERATION_NARGS("Mgr::varsInCommon");
  BDD common, onlyR, onlyS;
  r.ClassifySupport(s, &common, &onlyR, &onlyS);
  return !common.IsOne();
}

inline bool SpaceManager::variablesSubset(const BDD& r, const BDD& s) {
  LOG_OPERATION_NARGS("Mgr::varSubset");
  BDD common, onlyR, onlyS;
  r.ClassifySupport(s, &common, &onlyR, &onlyS);
  return common == r;
}

inline bool SpaceManager::isFalse(const BDD& r) {
  LOG_OPERATION_NARGS("Mgr::isFalse");
  // compute the terminal false without the need of the manager
  BDD False = r * ~r;
  return r == False;
}

inline bool SpaceManager::isTrue(const BDD& r) {
  LOG_OPERATION_NARGS("Mgr::isFalse");
  // compute the terminal false without the need of the manager
  BDD True = r + ~r;
  return r == True;
}

inline std::vector<BDD> SpaceManager::cubeToVector(BDD c) const {
  LOG_OPERATION_NARGS("Mgr::cubeToVector");
  // TODO: Is there a way to do this with a direct
  // call to the CUDD API?
  assert(c.IsCube());
  const auto& indices(c.SupportIndices());

  std::vector<BDD> vars;
  vars.reserve(c.SupportSize());
  for (auto v : indices)
    vars.push_back(bddVar(v));
  assert(vars.size() == static_cast<size_t>(c.SupportSize()));
  return vars;
}

namespace internal {
/**
 *@brief Checks whether @a n is a power of two.
 */
inline bool isPowerOfTwo(unsigned int n) { return !(n == 0) && !(n & (n - 1)); }
}
inline SpaceManager::SpaceManager(bool statsOnDestruction,
                                  unsigned int slotsInUniqueTable,
                                  unsigned int slotsInCache,
                                  unsigned long maxMemory,
                                  unsigned int cacheMinEfficiency,
                                  unsigned int maxSlotsInCache,
                                  bool dynamicReorder)
    : Cudd(0, // Number of initial BDD variables
           0, // Number of initial ZDD variables
           slotsInUniqueTable, slotsInCache, maxMemory)
    , uniqueAbstractCalls_(0)
    , fastUniqueAbstractCalls_(0)
    , statsOnDestruction_(statsOnDestruction) {
  Assert<InvalidPowerOfTwoValue>((internal::isPowerOfTwo(slotsInUniqueTable) &&
                                  internal::isPowerOfTwo(slotsInCache)),
                                 "SpaceManager::SpaceManager");
  if (cacheMinEfficiency != 0)
    SetMinHit(cacheMinEfficiency);
  if (maxSlotsInCache != 0)
    SetMaxCacheHard(maxSlotsInCache);
  if (dynamicReorder) {
    fmt::print_colored(fmt::BLUE, "Dinamyc reordering is enabled\n");
    AutodynEnable(CUDD_REORDER_SYMM_SIFT);
  }
}

inline SpaceManager::~SpaceManager(void) {
  fmt::print(stderr, "Finalizing BDD manager with {} references\n",
             zeroReferences());
  if (statsOnDestruction_) {
    info(std::cout);
    Cudd::info();
  }
}

inline void SpaceManager::info(std::ostream& os) const {
  fmt::print("Manager information:\n[Unique abstractions]: {}\n[Fast unique "
             "abstractions ]: {}\n[Manager used memoryMB]: {}\n[Cache "
             "lookups]: {}\n[Cache efficiency]: {}\n[GC steps]: {}\n[GC "
             "timeSec]: {}\n[Dead references]: {}\n",
             uniqueAbstractCalls_, memoryInUse(), cacheLookUps(),
             cacheEfficiency(), garbageCollections(), garbageCollectionTime(),
             zeroReferences());
}

inline int SpaceManager::zeroReferences(void) const {
  return Cudd_CheckZeroRef(getManager());
}

inline SpaceManager::BDDVarDomain SpaceManager::alloc(size_t length,
                                                      bool interleaved) {
  return allocator_.alloc(length, interleaved);
}

inline SpaceManager::BDDVarDomain
SpaceManager::alloc(const std::vector<int>& vars) {
  return allocator_.alloc(vars);
}

////////////////
// Statistics //
////////////////

inline unsigned long SpaceManager::memoryInUse() const {
  return ReadMemoryInUse() / 1048576.0;
}

inline double SpaceManager::cacheLookUps() const { return ReadCacheLookUps(); }

inline double SpaceManager::cacheEfficiency() const {
  return (ReadCacheHits() / ReadCacheLookUps());
}

inline int SpaceManager::garbageCollections() const {
  return ReadGarbageCollections();
}

inline long SpaceManager::garbageCollectionTime() const {
  return ReadGarbageCollectionTime();
}

//////////////
// Visitors //
//////////////

inline void cubeVisitor(BDD f, std::function<void(int*)> v) {
  int* cube; // this array will be allocated by the cube iterator
  CUDD_VALUE_TYPE value;
  auto* gen = f.FirstCube(&cube, &value);

  while (!Cudd_IsGenEmpty(gen)) {
    v(cube);
    Cudd_NextCube(gen, &cube, &value);
  }
  Cudd_GenFree(gen);
}

inline void assignementVisitor(int* minterm, unsigned int size,
                               const std::set<unsigned int>& filter,
                               std::function<void(int*)> visit,
                               unsigned int current) {
  if (current == size) {
    // base case, done
    visit(minterm);
  } else if (minterm[current] == 2 && filter.find(current) != filter.end()) {
    // It's a value we are interested in
    minterm[current] = 1;
    assignementVisitor(minterm, size, filter, visit, current + 1);

    minterm[current] = 0;
    assignementVisitor(minterm, size, filter, visit, current + 1);

    minterm[current] = 2;
  } else {
    // explore the next element in the cube
    assignementVisitor(minterm, size, filter, visit, current + 1);
  }
}
}

#endif
