#ifndef __AGG_RELATION_HH__
#define __AGG_RELATION_HH__

#include "agg-relation-decl.hh"
namespace CuddAbstraction {

/////////////////
// AggRelation //
/////////////////
inline AggRelation::AggRelation(void)
    : schema_()
    , data_() {}

inline AggRelation::AggRelation(ADD f, const Schema& sch)
    : schema_(sch)
    , data_(f) {}

inline AggRelation::AggRelation(const SpaceManager& home, const Schema& sch)
    : schema_(sch)
    , data_(home.addZero()) {}

inline AggRelation::AggRelation(const SpaceManager& home,
                                const std::vector<Attribute>& sch)
    : schema_(home, sch)
    , data_(home.addZero()) {}

inline AggRelation::AggRelation(const AggRelation& r)
    : schema_(r.schema_)
    , data_(r.data_) {}

inline AggRelation& AggRelation::operator=(const AggRelation& right) {
  if (this != &right) {
    if (!schema_.sameAs(right.schema()))
      throw InvalidSchema("AggRelation::operator=");
    data_ = right.data_;
    schema_ = right.schema_;
  }
  return *this;
}

inline double AggRelation::cardinality(void) const {
  int vars = schema_.asCube().SupportSize();
  return data_.CountMinterm(vars);
}

template <typename... Args>
void AggRelation::add(const SpaceManager& home, int agg, Args... args) {
  // TODO: this is not the most efficient way probably. I first build the BDD
  // that corresponds to the minterm without taking into account the
  // discriminant. The BDD is the transformed to an ADD and this is probably the
  // part that can be optimized. Then the ADD is multiplied by the minterm using
  // ADD times operation.
  assert(sizeof...(Args) == schema_.arity());

  BDD t = internal::encode(home, schema_, args...);
  ADD minterm = t.Add();
  minterm = minterm * home.constant(agg);
  data_ += minterm;
}

inline void AggRelation::add(const SpaceManager& home, int agg,
                             const NumericTuple& t) {
  assert(t.size() == schema_.arity());
  BDD tuple = schema_.asBDD(home, t);
  ADD minterm = tuple.Add();
  minterm = minterm * home.constant(agg);
  data_ += minterm;
}

inline AggRelation AggRelation::fromRelation(const Relation& r) {
  ADD data = static_cast<BDD>(r).Add();
  return AggRelation(data, r.schema());
}

inline AggRelation
AggRelation::groupBy(const SpaceManager& home, const Relation& r,
                     const std::vector<Attribute>& groupedAttributes) {
  Schema subsch(home, groupedAttributes);
  assert(r.schema().contains(subsch));
  Schema schemaToAbstract = r.schema().differenceWith(home, subsch);
  ADD cubeToAbstract = schemaToAbstract.asCube().Add();

  AggRelation agg = fromRelation(r);
  ADD abstractedData = agg.data_.ExistAbstract(cubeToAbstract);
  return AggRelation(abstractedData, subsch);
}

inline Relation AggRelation::filterInterval(const SpaceManager& home, int lb,
                                            int ub) const {
  BDD interval = data_.BddInterval(lb, ub);
  return Relation(interval, schema_);
}

inline Relation AggRelation::filterValue(const SpaceManager& home,
                                         int a) const {
  return filterInterval(home, a, a);
}

inline Relation AggRelation::aggregatedAsAttribute(const SpaceManager& home,
                                                   const Attribute& a) const {
  BDD result =
      home.augmentedWithDiscriminants(data_, a.asCube(), a.getIndices());
  Schema sch = schema_.unionWith(home, Schema(home, {a}));
  return Relation(result, sch);
}

inline int AggRelation::min(const SpaceManager& home) const {
  return home.value(data_.FindMin());
}

inline int AggRelation::max(const SpaceManager& home) const {
  return home.value(data_.FindMax());
}

inline int AggRelation::minNonzero(const SpaceManager& home) const {
  ADD nonZero = home.findMinNonzero(data_);
  return home.value(nonZero);
}

inline AggRelation AggRelation::joinWith(const SpaceManager& home,
                                         const Relation& r) const {
  const Schema& resultSch =
      (schema() != r.schema()) ? schema_.unionWith(home, r.schema()) : schema_;

  ADD rADD = static_cast<BDD>(r).Add();
  ADD result = rADD.Ite(data_, home.addZero());
  return AggRelation(result, resultSch);
}

inline void AggRelation::toDot(const SpaceManager& home, const char* fileName,
                               const char* funcName) const {
  DdNode* data = data_.getNode();
  const auto& names = schema_.levelNames(home);
  home.toDot(fileName, data, funcName, names);
}
}
#endif