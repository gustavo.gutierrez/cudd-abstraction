#ifndef __SCHEMA_DECL_HH__
#define __SCHEMA_DECL_HH__

#include "attribute-decl.hh"
#include "set-space-decl.hh"
#include "integer-datum-decl.hh"

#include <vector>

namespace CuddAbstraction {

class Schema {
  /// Collection of value domains
  std::vector<Attribute> domain_;
  /// Cube of variables representing the values in this domain
  BDD cube_;

public:
  // Default construction
  Schema(void);
  /// Constructor for an empty domain
  Schema(const SpaceManager& home);
  /// Assignment operator
  Schema& operator=(const Schema& right);
  /// Constructor from an array of value domains
  Schema(const SpaceManager& home, const std::vector<Attribute>& d);
  /// Copy constructor
  Schema(const Schema& d);

public:
  ////////////////////////
  // Schema information //
  ////////////////////////
  /**
   * @brief Returns the number of attributes in this schema
   */
  auto arity(void) const -> decltype(domain_.size()) { return domain_.size(); }
  /**
   * @brief Tests whether the represented schema is empty
   */
  bool isEmpty(void) const { return cube_.IsOne(); }
  /**
   * @brief Returns the number of BDD variables used to represent all the
   *     attributes in this schema.
   */
  size_t reprSize(void) const;

public:
  /// Adds @a vdom to be part of this domain
  void add(const Attribute& vdom);
  /**
   * @brief Creates a tuple encoding the elements of \a d.
   *
   * TODO: Raise an exception if the number of elements in d is not the same as
   *   the arity of this domain
   *
   * TODO: Raises an exception if it is not possible to represent an element of
   *    d in the size that the particular Attribute can store.
   *
   * Assumes that the elements belongs to a domain value by the order in the
   * array and the order in which the domains were specified on construction
   */
  BDD asBDD(const SpaceManager& home, const NumericTuple& d) const;
  /**
   * @brief Decodes the assignment \a a into a numeric tuple.
   *
   */
  NumericTuple decodeAssignment(int* a) const;
  /**
   * @brief Extracts the tuples represented in \a f as a collection of numeric
   *     tuples.
   */
  std::vector<NumericTuple> asTuples(const SpaceManager& home, BDD f) const;
  /**
   * @brief Returns a cube of the BDD variables used to represent all data that
   *     belongs to this schema.
   */
  BDD asCube(void) const { return cube_; }
  /**
   * @brief Outputs the BDD representation of the schema to @a fileName in DOT
   *     format.
   *
   * - If @a asADD is true then the equivalent ADD is printed instead of the
   *   BDD.
   * - @a funcName indicates the name of the root node
   */
  void toDot(const SpaceManager& home, const char* fileName,
             const char* funcName, bool asADD = true) const;
  /**
   * @brief Returns debug information about this schema.
   *
   * If @a varInfo is set to true variable indices is output.
   */
  std::string debugInfo(bool varInfo = false) const;

public:
  /////////////////////////////////
  // Attribute access operations //
  /////////////////////////////////
  /**
   * @brief Returns a collection with the corresponding column domains.
   */
  const std::vector<Attribute>& asColumnDomains(void) const { return domain_; }
  /* TODO: by adding the following method we could xlean up the relation code a
   * lillte bit. */
  // operator const std::vector<Attribute>&(void) const;
  /**
   * @brief Returns the attribute of this schema with name @a name
   *
   * If there is more than one attribute with the same name the first one found
   * is returned.
   *
   * TODO: throw an exception if there is no attribute called @a name in this
   * schema.
   */
  Attribute attribute(const std::string& name) const;

public:
  //////////////////////////////
  // Set operations on schema //
  //////////////////////////////
  /// Equality test
  bool sameAs(const Schema& d) const;
  /// Tests if this schema has at least one attribute in common with schema \a d
  bool overlapsWith(const Schema& d) const;
  /// Tests if this schema does not share any attribute with schema @a d
  bool disjointWith(const Schema& d) const;
  /// Tests if the attributes of this schema contains @a d
  bool contains(const Attribute& d) const;
  /// Tests if this schema contains all the attributes of @a d
  bool contains(const Schema& d) const;
  /**
   * @brief Returns the schema representing the union of this schema and schema
   *     @a d
   */
  Schema unionWith(const SpaceManager& home, const Schema& d) const;
  /**
   * @brief Returns the schema representing the difference between this schema
   *     and schema @a d
   */
  Schema differenceWith(const SpaceManager& home, const Schema& d) const;
  /**
   * @brief Returns the schema representing the intersection between this schema
   *     and schema @a d
   */
  Schema intersectWith(const SpaceManager& home, const Schema& d) const;
  /**
   * @brief Returns the schema representing the symmetric difference between
   *     this schema and schema @a d
   */
  Schema symDifferenceWith(const SpaceManager& home, const Schema& d) const;

public:
  /////////////////
  // Dot support //
  /////////////////
  /**
   * @brief Returns a mapping from BDD variables to strings
   *
   * This mapping is used to give each variable a name in the dot representation
   * of a relation. Moreover, we use levels instead of variables.
   */
  SpaceManager::LevelNamesMapType levelNames(const SpaceManager& home) const;
};

/**
 * @brief Tests if the the schemas @a r, @a s and @a t are set compatible
 */
bool setCompatible(const Schema& r, const Schema& s, const Schema& t);
/**
 * @brief Tests if the the schemas @a r and @a s are set compatible
 */
bool setCompatible(const Schema& r, const Schema& s);
/**
 * @brief Tests if the the schema @a t results from the joining of relations
 *     with schemas @a r and @a s
 */
bool joinCompatible(const SpaceManager& home, const Schema& r, const Schema& s,
                    const Schema& t);
/**
 * @brief Tests if the the schema @a t results from the composition of relations
 *     with schemas @a r and @a s
 */
bool composeCompatible(const SpaceManager& home, const Schema& r,
                       const Schema& s, const Schema& t);
/**
 * @brief Tests if the the schema @a t results from projecting a relation with
 *     schema @a r on a schema @a p
 */
bool projectCompatible(const Schema& r, const Schema& p, const Schema& t);
/**
 * @brief Test the schemas @a d and @a e for equality
 */
bool operator==(const Schema& d, const Schema& e);
/**
 * @brief Test the schemas @a d and @a e for dis-equality
 */
bool operator!=(const Schema& d, const Schema& e);
/**
 * @brief Outputs the schema @a d to @a os
 *
 * TODO: Is this operation really needed?
 */
std::ostream& operator<<(std::ostream& os, const Schema& d);
}

#endif